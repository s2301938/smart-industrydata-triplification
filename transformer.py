import pandas as pd
from rdflib import Graph, URIRef, Literal, Namespace
from rdflib.namespace import RDF, XSD
import sys
import os
#Get data from 
data_path = sys.argv[1]
# Load the dataset
#data_path = "C:\\Users\\Gebruiker\\Downloads\\household_data_60min_singleindex.csv"
household_data = pd.read_csv(data_path)


saref_path ="./saref.ttl"
g = Graph()
g.parse(saref_path, format='ttl')


SAREF = Namespace("https://saref.etsi.org/core/")
EX = Namespace("http://example.org/household/")

g.bind("saref", SAREF)
g.bind("ex", EX)

timestamp_column = 'utc_timestamp'
timestamp_column2 ='cet_cest_timestamp'
property_columns = [col for col in household_data.columns if col != timestamp_column or timestamp_column2]
#property_columns2 = [col for col in desandtype.columns if col != timestamp_column or timestamp_column2]
for index, row  in household_data.iterrows():
    timestamp = Literal(row[timestamp_column], datatype=XSD.dateTime)
    for col in property_columns:
        if pd.notna(row[col]):
            parent_node = URIRef(EX[col])
            reading = URIRef(EX[f"{col}_Reading_{index}"])
            
            g.add((parent_node, RDF.type, SAREF.Property))
            g.add((reading, RDF.type, SAREF.Measurement))
            g.add((reading, SAREF.hasTimestamp, timestamp))
            g.add((reading, SAREF.hasValue, Literal(row[col], datatype=XSD.string)))




desktop_path = os.path.join(os.path.expanduser('~'), 'Desktop')
output_folder = os.path.join(desktop_path, 'RDF_Output')
os.makedirs(output_folder, exist_ok=True)  

output_path = os.path.join(output_folder, 'graph_sample.ttl')
g.serialize(destination=output_path, format='turtle')

print(f"RDF graph serialized and saved to {output_path}")

